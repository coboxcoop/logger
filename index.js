const debug = require('debug')
const pino = require('pino')
const pinoHttp = require('pino-http')
const crypto = require('@coboxcoop/crypto')
const { isString } = require('util')

const REDACTED_FIELDS = [
  '*.name',
  '*.address',
  '*.encryptionKey',
  '*.discoveryKey',
  '*.code',
  '*.author',
  '*.publicKey'
]

module.exports = function log (name) {
  if (!isProduction()) return debug(name)
  const logger = pino({ name })
  return (...args) => logger.info(args.map(deepRedact))
}

module.exports.requestLogger = function requestLogger (name) {
  const logger = pino({ name })
  return pinoHttp({
    logger,
    redact: {
      paths: REDACTED_FIELDS,
      censor: redact
    },
    serializers: {
      req: (req) => {
        req.url = req.url.split('/').map((namespace) => {
          if (isHexString(namespace, 64)) return hex(redact(Buffer.from(namespace), 32))
          return namespace
        }).join('/')
        return req
      }
    }
  })
}

function isProduction () {
  return process.env.NODE_ENV === 'production'
}

function isHexString (str, length) {
  if (!isString(str)) return false
  if (length && (str.length !== length)) return false
  return RegExp('[0-9a-fA-F]+').test(str)
}

function hex (buf) {
  if (Buffer.isBuffer(buf)) return buf.toString('hex')
  return buf
}

function redact (buf, len = 32) {
  if (!Buffer.isBuffer(buf)) buf = Buffer.from(buf)
  return crypto.genericHash(buf, null, len)
}

function deepRedact (obj) {
  if (Array.isArray(obj)) return obj.map(deepRedact)

  if (obj != undefined && typeof obj === 'object') return Object.keys(obj)
    .filter(k => obj[k] != null)
    .reduce((acc, key) => {
      if (!Buffer.isBuffer(obj[key]) && typeof obj[key] === 'object') return { ...acc, [key]: deepRedact(obj[key]) }
      if (isHexString(obj[key], 64)) return { ...acc, [key]: hex(redact(obj[key])) }
      if (crypto.isKey(obj[key])) return { ...acc, [key]: hex(redact(obj[key])) }
      if (isString(obj[key]) && key === 'name') return { ...acc, [key]: hex(redact(obj[key], 16)) }
      return { ...acc, [key]: obj[key] }
    }, {})

  if (isHexString(obj, 64)) return hex(redact(obj))
  if (Buffer.isBuffer(obj) && crypto.isKey(obj)) return hex(redact(obj))
  return obj
}
